﻿<?php

  /**
  * @file
  * Allows you to define limits for users, given their roles and the node types.
  */
	define("CONTENT_LIMIT_PERM_ADMIN", "administer content limits");
	
	/**
	* Implementation of hook_help()
	*/	
	function content_limit_help($path, $arg) {
	  switch ($path) {
	    case 'admin/help#content_limit':
        $output = '';
        $output .= '<h3>' . t('About') . '</h3>';
        $output .= '<p>' .  t('The Content Limit module allows you to define limits for users, given their roles and the node types.<br> If no limit is defined, users can create as many nodes as Drupal allows them to.'). '</p>';
        $output .= '<h3>' . t('Uses') . '</h3>';
        $output .= '<dl>';
        $output .= '<dt>' . t('Add content limits') . '</dt>';
        $output .= '<dd>' . t('To add a new content limit, visit the <a href="@content_limits">Content Limits</a> page, and click the <em>add</em> link to add a configuration.', array('@content_limits' => url('admin/structure/content_limit'))) . '</li></ul><dd>';
        $output .= '<dt>' . t('Assigning priorities to your limits') . '</dt>';
        $output .= '<dd>' . t("In the <a href='@content_limits'>Content Limits</a> page, use the drag button to sort your limits and define their priorities.<br>Don't forget to save your limits by clicking the <em>save</em> button after sorting them.", array('@content_limits' => url('admin/structure/content_limit/page'))) . '</dd>';	
        $output .= '<dt>' . t('Disabling your limits') . '</dt>';
        $output .= '<dd>' . t("By default, the limits you create are enabled, you can disable them by clicking the <em>active</em> button in the <a href='@content_limits'>Content Limits</a> page.<br>Don't forget to save your limits after enabling/disabling them.", array('@content_limits' => url('admin/structure/content_limit/page'))) . '</dd>';
        $output .= '</dl>';
        return $output;
	  }
	}	
	
	/**
	 * Implements hook_admin_paths().
	 */
	function content_limit_admin_paths() {
	  $paths = array(
	    'admin/structure/' 						          => TRUE,
	    'admin/structure/content_limit/page'	  => TRUE,
      'admin/structure/content_limit/add' 	  => TRUE,
      'admin/structure/content_limit/%/delete'=> TRUE,
      'admin/structure/content_limit/%/clone' => TRUE,
      'admin/structure/content_limit/%' 		  => TRUE,
      'admin/help#content_limit' 				      => TRUE,
		);
		return $paths;
	}	
	
	/**
	 * Implements hook_menu_alter().
	 */
	function content_limit_menu_alter(&$items) {
	  node_type_cache_reset();
	  $items['node/add']['access callback'] = TRUE;
	}	
	
	/**
	 * Implements hook_permission().
	 */
	function content_limit_permission() {
	  return array(
      CONTENT_LIMIT_PERM_ADMIN => array(
        'title' 		  => t('Administer content limits'),
        'description' => t('Allow administrators to change the content limit values')
      )
	  );
	}	
	
	/**
	* Implementation of hook_menu()
	*/	
	function content_limit_menu() {
	  $items=array();		
	  $items['admin/structure/content_limit'] = array(
      'title'          	=> 'Content Limits',
      'description'    	=> t('Administer your content limits'),
      'page callback'  	=> 'drupal_get_form',
      'page arguments'  => array('content_limit_list_limits'),
      'access callback' => TRUE,
	  );		
	  $items['admin/structure/content_limit/page'] = array(
      'title' 			    => 'Content Limits',
      'description' 		=> t('Administer your content limits.'),
      'page callback' 	=> 'drupal_get_form',
      'type'				    => MENU_CALLBACK,
      'page arguments'	=> array('content_limit_list_limits'),
      'access arguments'=> array('CONTENT_LIMIT_PERM_ADMIN'),
	  );			
	  $items['admin/structure/content_limit/add'] = array(
      'title' 			    => t('Add Content Limit'),
      'description'		  => t('Add a new limit'),
      'page callback'		=> 'drupal_get_form',
      'page arguments'	=> array('content_limit_add_form' , 3),
      'type'				    => MENU_CALLBACK,
      'access arguments'=> array('CONTENT_LIMIT_PERM_ADMIN'),
	  );			  
	  $items['admin/structure/content_limit/%/delete'] = array(
      'title' 		      => t('Delete Content Limit'),
      'page callback' 	=> t('drupal_get_form'),
      'page arguments'	=> array('content_limit_delete_form' , 3),
      'access arguments'=> array('CONTENT_LIMIT_PERM_ADMIN'),
      'type' 				    => MENU_CALLBACK,
	  );
	  $items['admin/structure/content_limit/%/clone'] = array(
      'title'	 			    => t('Clone Content Limit'),
      'page callback' 	=> 'content_limit_clone',
      'page arguments' 	=> array(3),
      'type' 				    => MENU_CALLBACK,
      'access arguments'=> array('CONTENT_LIMIT_PERM_ADMIN'),
	  );
	  $items['admin/structure/content_limit/%'] = array(
      'title' 			    => t('Edit Content Limit'),
      'page callback'		=> 'drupal_get_form',
      'page arguments'	=> array('content_limit_add_form' , 3),
      'type'				    => MENU_CALLBACK,
      'access arguments'=> array('CONTENT_LIMIT_PERM_ADMIN'),
	  );
	  return $items;
	}

	function content_limit_theme() {
	  return array(
      'content_limit_list_limits' => array(
        'render element' 			    => 'form',
      )
	  );
	}
	
	/**
	* Theming the tab that lists limits
	*/
	function theme_content_limit_list_limits($variables) {
	  $form = $variables['form'];
	  $rows = array();		  
	  foreach (element_children($form['limits']) as $key) {
      if (isset($form['limits'][$key]['title'])) {
        $limit = &$form['limits'][$key];
        $row = array();
        $row[] = drupal_render($limit['title']);
        $row[] = drupal_render($limit['roles']);
        $row[] = drupal_render($limit['contents']);
        if (module_exists('domain')) {
          $row[] = drupal_render($limit['domains']);
        }
        $row[] = drupal_render($limit['limit']);
        $row[] = drupal_render($limit['active']);          
        if (isset($limit['weight'])) {
          $limit['weight']['#attributes']['class'] = array('content_limit-weight');
          $row[] = drupal_render($limit['weight']);
        }          
        $row[] = drupal_render($limit['edit']);
        $row[] = drupal_render($limit['list']);
        $row[] = drupal_render($limit['clone']);
        $rows[] = array('data' => $row , 'class' => array('draggable'));
      }
	  }		
	  $header = array(t('Title'));
	  $header[] = t('Roles');
	  $header[] = t('Contents');
    if (module_exists('domain')) {
      $header[] = t('Domains');
    }
	  $header[] = t('Limit');
	  $header[] = t('Active');		
	  if (isset($form['save'])) {
      $header[] = t('Weight');
      drupal_add_tabledrag('content_limit', 'order', 'sibling', 'content_limit-weight');
	  }		
	  $header[] = array('data' => t('Actions') , 'colspan' => '3');
	  return drupal_render($form['create']).theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No limit available.') . ' ' . drupal_render($form['create']), 'attributes' => array('id' => 'content_limit'))). drupal_render_children($form) ;
	}	

	/**
	* Tab that lists limits
	*/
	function content_limit_list_limits() {
	  $form = array('#tree' => TRUE);
	  $form['limits'] = array();
	  // Getting the content_limit attributes
	  $query = db_select('content_limit', 'cl')
      ->fields('cl')
      ->orderBy('config_priority', 'ASC')
      ->execute();		
    // Getting all the roles as an array ( $role_id => $role_name )
	  $all_roles = user_roles($membersonly = FALSE, $permission = NULL);    
	  $nlimit = 0;
	  // For each content_limit
	  foreach ($query as $row) {
      $nlimit++;      
      $form['limits'][$row -> config_id]['weight'] = array(
        '#type' => 'weight',
        '#default_value' => $row -> config_priority,
      );      
      $form['limits'][$row -> config_id]['title'] = array(
        '#markup' => check_plain($row -> title),
      );
      $form['limits'][$row -> config_id]['limit'] = array(
        '#markup' => check_plain($row -> content_limit),
      );      
      $roles_display = array();
      $rid_exploded = array();
      // Getting users roles ids as an array ( $key => $rid )
      $rid_exploded = explode(',' , $row -> user_roles);      
      // Loops in the users roles array
      foreach ($rid_exploded as $key => $rid) {      
        // Adds ' - ' and '<br>' for the display
        $roles_display[] = '-  ' . $all_roles[$rid_exploded[$key]] . '<br>';      
      }      
      // Getting users roles as a list
      $roles = implode($roles_display);
      // Displays the list
      $form['limits'][$row -> config_id]['roles'] = array(
        '#markup' => $roles,
      );      
      $content_exploded = array();
      // Getting content types as an array
      $content_exploded = explode(',' , $row->content_types);
      // Loops in the content types array
      foreach ($content_exploded as $key => $content) {
        // Adds ' - ' and '<br>' for the display
        $content_exploded[$key] = '-  ' . $content_exploded[$key] . '<br>';
      }
      // Getting content types as a list
      $contents = implode($content_exploded);
      // Displays the list
      $form['limits'][$row -> config_id]['contents'] = array(
        '#markup' => $contents,
      );		
      if (module_exists('domain')) {
        // Getting all the domains as an array ($domain_name => $domain_id)
        $all_domains = domain_id_list($reset = FALSE);
        $all_domains = array_flip($all_domains);
        $domain_display = array();
        $domain_exploded = array();
        // Getting domains as an array
        $domain_exploded = explode(',' , $row -> domains_id);
        // Loops in the domains array
        foreach ($domain_exploded as $key => $domain_id) {
          // Adds ' - ' and '<br>' for the display
          $domain_display[] = '-  ' . $all_domains[$domain_exploded[$key]] . '<br>';
        }
        // Getting domains as a list
        $domains = implode($domain_display);
        // Displays the list
        $form['limits'][$row -> config_id]['domains'] = array(
          '#markup' => $domains,
        );
      }		
      $active = 1;
      if ($row -> active == 0) {
        $active = 0;
      }
      $form['limits'][$row -> config_id]['active'] = array(
        '#type' 			  => 'checkbox',
        '#default_value'=> $active,
      );
      $form['limits'][$row -> config_id]['edit']  = array('#type' => 'link' , '#title' => t('Edit') ,   '#href' => 'admin/structure/content_limit/' . $row -> config_id);
      $form['limits'][$row -> config_id]['list']  = array('#type' => 'link' , '#title' => t('Delete') , '#href' => 'admin/structure/content_limit/' . $row -> config_id . '/delete');
      $form['limits'][$row -> config_id]['clone'] = array('#type' => 'link' , '#title' => t('Clone') ,  '#href' => 'admin/structure/content_limit/' . $row -> config_id . '/clone');
	  }
	  $form['create'] = array(
	  	'#type' => 'link',
      '#title'=> t('Add'),
      '#href' => 'admin/structure/content_limit/add',
	  );
	  if ($nlimit > 0) {
      $form['save'] = array(
        '#type'  => 'submit',
        '#value' => t('Save'),
      );
	  }
	  return $form;
	}

	/**
	* The tab submit (sorting priorities, enabling/disabling limits)
	*/	
	function content_limit_list_limits_submit($form_id, &$form_state) {
	  foreach ($form_state['values']['limits'] as $config_id => $info) {
      if ($info['weight'] < 0 ) {
        $info['weight'] += 10;  // 10 = #delta default value (from -10 to 10), if negative, +10 to start with 0
      }
      $query = db_update('content_limit')
        ->fields(array(
          'config_priority' => $info['weight'],
          'active'          => $info['active'],
        ))
        ->condition('config_id', $config_id)
        ->execute();
	  }
	  drupal_set_message(t('Limits saved!'));
	}	
	
	/**
	* Delete page
	*/	
	function content_limit_delete_form($form, $form_state, $config) {
	  $result = db_query("SELECT config_id FROM {content_limit} WHERE config_id = '$config'");
	  foreach ($result as $record) {
      $id = $record -> config_id;
	  }	
	  $query = db_select('content_limit' , 'cl')
      ->fields('cl')
      ->condition('config_id', $id)
      ->execute();			
		foreach ($query as $row) {
		  $title = $row->title;
		}		
		$form = array(
		  'config_id' => array(
        '#type' 	=> 'hidden',
        '#value'  => $id,
		  )
		);
	  return confirm_form($form, t('Are you sure you want to delete %name?', array('%name' => $title)), 'admin/structure/content_limit/page');
	}	
	
	/**
	* Deletes a limit from the database 
	*/	
	function content_limit_delete_form_submit($form_id, &$form_state) {
	  $config_id = $form_state['values']['config_id'];
	  $num = db_delete('content_limit')
      ->condition('config_id' , $config_id)
      ->execute();
	  drupal_goto('admin/structure/content_limit/page');
	}
	
	/**
	*  Add/Edit form
	*/	
	function content_limit_add_form($form, &$form_state, $config = FALSE) {	  
	  // Getting all the roles (only roles IDs are stored in the database)
	  $all_roles = user_roles($membersonly = FALSE, $permission = NULL);
	  foreach ($all_roles as $key => $role) {
	    $roles[$key] = $role;
	  }		
	  // Getting all the node types	  
	  $all_types = node_type_get_types();
	  foreach ($all_types as $type => $value) {
      $content_types[$type] = $type;
	  }	  
	  // Initialization of default values
	  $id     = 0;
	  $title  = '';
	  $limit  = '';
	  $nodes1 = '';
	  $user1  = '';
	  $user   = array();
	  $nodes  = array();	  
	  // If editing, we catch the limit attributes to display them in the form
	  if ($config) {
	    $result = db_query("SELECT config_id, title, user_roles, content_types, content_limit, domains_id FROM {content_limit} WHERE config_id = '$config'");
	    foreach ($result as $record) {
        $id     = $record -> config_id;
        $title  = $record -> title;
        $limit  = $record -> content_limit;
        $user1  = $record -> user_roles;
        $nodes1 = $record -> content_types;
	    }
	    $nodes = explode(',' , $nodes1);
      $user =  explode(',' , $user1);
	  }
	  // Displays the form
	  $form = array();
	  $form['title'] = array(
      '#type' 		    => 'textfield',
      '#title' 		    => t('Give your limit a title'),
      '#default_value'=> $title,
      '#required'		  => TRUE,
	  );
	  $form['user_roles'] = array(
      '#type' 		    => 'checkboxes',
      '#title' 		    => t('Select the role(s) involved in the limit'),
      '#options' 		  => $roles,
      '#default_value'=> $user,
      '#required' 	  => TRUE,
	  );	 
	  $form['content_types'] = array(
      '#type'			    => 'checkboxes',
      '#title' 		    => t('Select the content(s) involved in the limit'),
      '#options' 		  => $content_types,
      '#default_value'=> $nodes,
      '#required' 	  => TRUE,
	  );
	  // If the Domain Access module is enabled, we add new checkboxes
	  if (module_exists('domain')) {
	    // Getting all the domain names
	    $domain = domain_id_list($reset = FALSE);
      $domains = array();
      foreach ($domain as $domain_name => $domain_id) {
        $domains[$domain_id] = $domain_name;
      }		
      $domains_default = array();
      $domains_defined = '';
      // If editing, we catch the domains already defined in the limit
      if ($config) {
        $result = db_query("SELECT domains_id FROM {content_limit} WHERE config_id = '$config'");
        foreach ($result as $record) {
          $domains_defined = (string) $record -> domains_id;
        }
        $domains_default = explode(',' , $domains_defined);
      }	  
	    $form['domain'] = array(
        '#type'			    => 'checkboxes',
        '#title' 	    	=> t('Select the domain(s) involved in the limit'),
        '#options' 		  => $domains,
        '#default_value'=> $domains_default,
        '#required'   	=> TRUE,
	    );
	  }		
	  $form['limite'] = array(
      '#type' 		    => 'textfield',
      '#title' 		    => t('Enter the limit to apply'),
      '#default_value'=> $limit,
      '#required' 	  => TRUE,
	  );
	  $form['submit'] = array(
      '#type' 	=> 'submit',
      '#value' 	=> t('Save'),
	  );	  
	   $form['id'] = array (
      '#type' => 'hidden',
      '#value'=> $id,
	  );
	  return $form;
	}	
	
	/**
	* Checks the values entered by the user
	*/	
	function content_limit_add_form_validate($form, $form_state) {
	  if (!is_numeric($form_state['values']['limite'])) {
      form_set_error('info][limit', t('Node limits must be an integer'));
	  }
	  if (($form_state['values']['limite']) <= 0) {
      form_set_error('info][limit', t('Node limits must be greater than 0'));
	  }
	}
	
	/**
	* Adds/Edits a limit in the database
	*/ 
	function content_limit_add_form_submit($form, $form_state) {
     // Getting the lowest priority (the highest priority is 0)
	  $query = 'SELECT MAX(config_priority) FROM {content_limit}';
	  $nb_config = db_query($query) -> fetchField();
	  if ($nb_config == NULL) {
      $nb_config = 0;
	  }
	  else {
      $nb_config++;
	  }		
	  $title = 		     $form_state['values']['title'];
	  $user_roles =    $form_state['values']['user_roles'];
	  $content_types = $form_state['values']['content_types'];
	  $content_limit = $form_state['values']['limite'];
	  $domain = (isset($form_state['values']['domain'])) ? ($form_state['values']['domain']) : 'none';
    foreach ($user_roles as $key => $value){
      if ($user_roles[$key] == '0'){
        unset($user_roles[$key]);
      }
    }
    foreach ($content_types as $key => $value){
      if ($content_types[$key] == '0'){
        unset($content_types[$key]);
      }
    }
    foreach ($domain as $key => $value){
      if ($domain[$key] == '0'){
        unset($domain[$key]);
      }
    }    
	  $user_roles = 	implode(',' , $user_roles);
	  $content_types =implode(',' , $content_types);
    $domain =       implode(',' , $domain);    
	  // If add :
	  if ($form_state['values']['id'] == 0) {
	    $nid = db_insert('content_limit')
        ->fields(array(
          'title' 			    => $title,
          'user_roles' 		  => $user_roles,
          'content_types'	  => $content_types,
          'content_limit' 	=> $content_limit,
          'active' 			    => 1,
          'domains_id'	    => $domain,
          'config_priority' => $nb_config, // The lowest priority
        ))
        ->execute();
	  }	  
	  // If edit :
	  else {
	    $id = $form_state['values']['id'];
      $nid = db_update('content_limit')
        ->fields(array(
          'title' 		    => $title,
          'user_roles' 	  => $user_roles,
          'content_types' => $content_types,
          'content_limit' => $content_limit,
          'domains_id'	  => $domain,
        ))
        ->condition('config_id', $id )
        ->execute();
	  }
	  drupal_set_message($message = t('The limit has been saved succesfully') , $type = 'status' , $repeat = TRUE);
	  drupal_goto('admin/structure/content_limit/page');
	}

	/**
	* Clones a limit
  *
  * @param int $config
  *   The id of the config that needs to be cloned
	*/	
	function content_limit_clone($config) {
	  $query = db_select('content_limit', 'cl')
      ->fields('cl')
      ->condition('config_id', $config)
      ->execute();			
	  $query2='SELECT MAX(config_priority) FROM {content_limit}';
	  $max_config = db_query($query2)->fetchField();		
	  foreach ($query as $row) {
      $title 		= 'Clone de '. $row -> title;
      $roles  	= $row -> user_roles;
      $contents = $row -> content_types;
      $limit 	  = $row -> content_limit;
      $active 	= $row -> active;
      $domain 	= $row -> domains_id;
	  }		
	  $clone = db_insert('content_limit')
      ->fields(array(
        'title' 			    => $title,
        'user_roles'		  => $roles,
        'content_types'	  => $contents,
        'content_limit'   => $limit,
        'active' 			    => $active,
        'config_priority'	=> $max_config + 1,
        'domains_id'	    => $domain,
      ))
      ->execute();
	  drupal_set_message(t('Cloned limit!'));
	  drupal_goto('admin/structure/content_limit/page');
	}
	
	/**
	* Implementation of hook_node_prepare()
  * This function checks if the user is allowed to create the node
	*/	
	function content_limit_node_prepare($node) {
	  global $user;
	  $config = array();
    // Getting the limits
	  $query = 'SELECT * FROM {content_limit} WHERE active <> 0 ORDER BY config_priority ASC';
	  $result = db_query($query)->fetchAllAssoc('config_priority');		
	  foreach ($result as $row) {
		$config[$row->config_priority] = array( 'user_roles' 			=> $row -> user_roles,
                                            'content_types'   => $row -> content_types,
                                            'limit' 		      => $row -> content_limit,
                                            'priority'	    	=> $row -> config_priority,
                                            'active'		      => $row -> active,
                                            'domain'		      => $row -> domains_id,
                                          );
	  }		
	  // Node type the user wants to create
	  $content_type = $node -> type;
	  // User roles
	  $role = $user -> roles;
	  // User id
	  $uid = $user -> uid;	  
    // Getting the number of nodes already created by the user
	  $nodes = entity_load($entity_type = 'node', $ids = FALSE, $conditions = array('type' => $content_types , 'uid' => $uid), $reset = FALSE);    
    $b = 'domains';    // Useful to get $nodes[$key]->'domains'    
	  // If Domain Access is enabled
	  if (module_exists('domain')) {
	    global $_domain;
	    $domain = $_domain['domain_id'];
      // We will only count the nodes created in the actual domain
      foreach ($nodes as $key => $value) {
        if (!in_array($domain, $nodes[$key]->$b)){
          unset ($nodes[$key]);
        }
      }
	  }
	  else {
      $domain = 'none';
	  }
    $nb = count($nodes);  
	  // For each limit
	  foreach ($config as $cle_config => $valeur_config) {
      // Getting the roles ids defined in the limit, as an array
      $roles_config = explode(',' , $config[$cle_config]['user_roles']);
      // For each user role
      foreach ($role as $cle_role => $valeur_role) {
        // If the role is defined in the limit
        if (in_array($cle_role , $roles_config)) {
        // And if the type is defined
          if (content_limit_type_in_config($content_type , $config[$cle_config]['content_types'])) {
            // And if the domain is also defined in the limit
            if (content_limit_domain_in_config($domain , $config[$cle_config]['domain'])) {
              // We check if the limit is not reached
              if (content_limit_check_limit($nb , $config[$cle_config]['limit'])) {
                return TRUE; // All the conditions are OK, the user can create the node
              }
              else {
                $erreur = t("You can't create this node, you have already posted $nb $content_types(s).");
                drupal_set_message($message = $erreur , $type = 'error' , $repeat = TRUE);
                drupal_goto('node/add');
              }
            }
          }
        }
      }
	  }
	}	
	
	/**
	* Checks if the limit has not been reached
  *
  * @param int $nb
  *   The number of nodes the user already has created
  *
  * @param int $nb_limit
  *   The limit defined in the configuration
  *
  * @return bool
  *   Indicates whether $nb is bigger than $nb_limit or not
  *   
	*/	
	function content_limit_check_limit($nb , $nb_limit ) {
	  if ($nb < $nb_limit) {
      return TRUE;
	  }
	  return FALSE;
	}	
	
	/**
	* Checks if, for the actual limit, the actual type is defined
  *
  * @param string $content_type
  *   The content type the user want to create
  *
  * @param array $contents_defined
  *   An array containing the contents types defined in the current configuration
  *
  * @return bool
  *   Indicates whether $content_type is defined in the configuration
	*/	
	function content_limit_type_in_config($content_type , $contents_defined) {
	  $types_config = explode("," , $contents_defined);
	  if (in_array($content_type , $types_config)) {
      return TRUE;
	  }
	  return FALSE;
	}	
	
	/**
	* Checks if, for the actual limit, the actual domain is defined
  *
  * @param string $domain
  *   The current domain
  *
  * @param array $domain_defined
  *   An array containing the domains defined in the current configuration
  *
  * @return bool
  *   Indicates whether the $domain is defined in the configuration
	*/	
	function content_limit_domain_in_config($domain , $domain_defined) {
	  $domains_config = explode("," , $domain_defined);
	  if (in_array($domain , $domains_config)) {
      return TRUE;
	  }
	  return FALSE;
	}